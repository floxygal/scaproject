terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket = "scaproject"
    key    = "tf-statefile"
    region = "us-east-1"
}
 required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-west-1"
  }

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCqixnR+xzl6dJres1NoPW77/zfWVWltTEmuno71vMFwlL9Q9JLpFO/Ihc77Peiy1L3THgVfnA9gceTtNR6N3gptbyDCrcbZ4kQrK0Joz75QVzKV9bvRn8EZVXlMlS5xsnG00PKChC7Ly9FHqKil4zcfvHhR6K74AynwacDcEOkc3Dxc01Ht/EUpLspGHRNiC2BODDXzDldDqE0JJehAErPrB3vHBABYi6yjhDF2bnuhK8X3WxqGXhus+FAJsCLzTileidN/95y19g71sB87VIEYrk9F0AIJbOYPv4Y2cOya78lRP8XIKE6OITs9x0kkbuZfZxjO5uM1J10LeQFwF4irrNkoru/4JphyUQZfMGibfFoniBzzVWVvw9i7wgSCf2uwhgoxyhlozRVNnSZ2MeW5QdqmKxqluBYZie8rVA8Eet9XGzRGuQUfWNvPqYMtQp34IdXDnJoGeJ5uNUn2bfxd0KqX/MT7o2VbY51JVZoS78Eb/YoVsd63QnwfQao6G0= FLORENCE@DESKTOP-BNNEIHK"
}

resource "aws_instance" "serverone" {
  ami           = "ami-0f7cd40eac2214b37"
  instance_type = "t2.micro"
  count         = 2
  key_name      = aws_key_pair.deployer.key_name

  tags = {
    Name = "serverone"
  }

}

output "instance_ips" {
  value = aws_instance.app_server.*.public_ip
}

